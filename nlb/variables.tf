variable "ports"{
    type   = map(number)
    default = {
        http = 80
        https = 443
    }
}

variable "vpc_id" {
  type        = string
  default     = "vpc-d461f3b3"
  description = "VPC ID to associate with ALB"
}

variable "subnet_ids" {
  default     = ["subnet-97e4e9aa", "subnet-40fb1b09"]
  type        = list(string)
  description = "A list of subnet IDs to associate with ALB"
}

variable "subnet_id" {
  type        = string
  description = "Subnet ID to associate with ALB"
}

variable "security_group_ids" {
  type        = list(string)
  default     = [""]
  description = "A list of additional security group IDs to allow access to ALB"
}

variable "deletion_protection_enabled" {
  type        = bool
  default     = false
  description = "A boolean flag to enable/disable deletion protection for ALB"
}

variable "target_group_port" {
  type        = number
  default     = 80
  description = "The port for the default target group"
}

variable "target_group_protocol" {
  type        = string
  default     = "HTTP"
  description = "The protocol for the default target group HTTP or HTTPS"
}

variable "target_group_protocol_version" {
  type        = string
  default     = "HTTP1"
  description = "The protocol version for the default target group HTTP1 or HTTP2 or GRPC"
}

variable "target_group_name" {
  type        = string
  default     = "tg-testing123"
  description = "The name for the default target group, uses a module label name if left empty"
}


variable "target_group_target_type" {
  type        = string
  default     = "instance"
  description = "The type (`instance`, `ip` or `lambda`) of targets that can be registered with the target group"
}

variable "stickiness" {
  type = object({
    cookie_duration = number
    enabled         = bool
  })
  description = "Target group sticky configuration"
  default     = null
}

variable "sg_name" {
  type        = string
  default     = "test123"
  description = "Name of security group"
}

variable "autoscaling_group_name" {
    type       = string
    default    = "as-test123"
    description  = "Name of ASG that instances belong to"
}

variable "load_balancer_name" {
    type         = string
    default      = "nlb-test123"
}