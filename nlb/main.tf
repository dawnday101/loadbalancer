resource "aws_lb_target_group" "test" {
  for_each                = var.ports

  port                    = each.value
  protocol                = "TCP"
  vpc_id                  = var.vpc_id

  depends_on              = [aws_lb.test]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_target_group_attachment" "instance1" {
  target_id               = var.target_id1
  port                    = 80
  lb_target_group_arn     = aws_lb_target_group.test.arn
  depends_on              = [aws_lb_target_group.test]
}

resource "aws_target_group_attachment" "instance1" {
  target_id               = var.target_id2
  port                    = 80
  lb_target_group_arn     = aws_lb_target_group.test.arn
  depends_on              = [aws_lb_target_group.test]
}

resource "aws_lb" "test" {
  name                    = var.load_balancer_name
  internal                = false
  load_balancer_type      = "network"
  subnets                 = var.subnet_ids

  enable_cross_zone_load_balancing = true

  tags = {
    Environment           = "dev"
  }
}

resource "aws_lb_listener" "test" {
  for_each                = var.ports

  load_balancer_arn       = aws_lb.test.arn

  protocol                = "TCP"
  port                    = each.value 

  default_action {
    type                  = "forward"
    target_group_arn      = aws_lb_target_group.test[each.key].arn
  }
}

resource "aws_security_group" "test" {
  description             = "Allow connection between NLB and target"
  vpc_id                  = var.vpc_id
}

resource "aws_security_group_rule" "ingress" {
  for_each                = var.ports
  security_group_id       = aws_security_group.test.id

  from_port               = each.value
  to_port                 = each.value
  protocol                = "tcp"
  type                    = "ingress"
  cidr_blocks             = ["0.0.0.0/0"]
}

